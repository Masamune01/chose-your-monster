import React from 'react';

export const SearchBar = () => {
    return(
        <div className="container">
            <input class='form-control' 
                type='text' 
                placeholder='Cosa ti piacerebbe cercare?' 
                style={{margin: '20px'}}
                />
        </div>
    )
};