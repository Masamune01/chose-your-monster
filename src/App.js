import React, {Component} from 'react';
import './App.css';
import {CardList} from './components/card-list/card-list.component'
import { SearchBar } from './components/searchbar';
class App extends Component{
constructor(){
  super();
  this.state = {
    monsters: []
  }
}

componentDidMount(){
  fetch('https://picsum.photos/v2/list') 
  .then(response => response.json())
  .then(list => this.setState({monsters: list}))
}


 render(){
  return (
    <div classname='container-fluid'>
      <div className="App">
      <SearchBar />
      <CardList monsters = {this.state.monsters} />
      </div>
    </div>
  );
}
}
export default App;
