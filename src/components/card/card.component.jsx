import React from 'react';
import './card.styles.css'

export const Card = (props) => {
    console.log(props.monsters)
    return(
        <div className='card-container'> 
            <img alt={props.monsters.id} src={props.monsters.download_url} width='180' height='180' />
            <h2>{props.monsters.author}</h2>
            <p>L'id dell'immagine è: {props.monsters.id}</p> 
        </div>
    )
}